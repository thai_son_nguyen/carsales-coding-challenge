using FluentAssertions;
using FluentAssertions.Execution;
using Newtonsoft.Json;
using SalesPersonAllocation.Domain.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Repository.Test
{
    public class SalesPersonRepositoryTests
    {
        public class GetAll
        {
            [Fact]
            public async Task ShouldLoadAllData()
            {
                var dataFile = Path.Combine(Directory.GetCurrentDirectory(), "TestData", "salespersonTests.json");

                var repo = new SalesPersonRepository(dataFile);

                var persons = (await repo.Entities()).ToList();

                using (new AssertionScope())
                {
                    var person = persons[0];
                    person.Name.Should().Be("Cierra Vega");
                    string.Join(",", person.Groups).Should().Be("A");
                    person.IsAssigned.Should().Be(false);

                    person = persons[1];
                    person.Name.Should().Be("Alden Cantrell");
                    string.Join(",", person.Groups).Should().Be("B,D");
                    person.IsAssigned.Should().Be(true);
                }
            }
        }

        public class SaveChanges
        {
            [Fact]
            public async Task ShouldSaveAllChanges()
            {
                var dataFile = Path.Combine(Directory.GetCurrentDirectory(), "TestData", "testSaves.json");

                var salesPersons = new List<SalesPerson>
                {
                    new SalesPerson
                    {
                        Name = "P1",
                        Groups = new List<string> { "A" }
                    }
                };

                using (StreamWriter writer = File.CreateText(dataFile))
                {
                    await writer.WriteAsync(JsonConvert.SerializeObject(salesPersons));
                }

                var repo = new SalesPersonRepository(dataFile);

                salesPersons = (await repo.Entities()).ToList();

                salesPersons.First().Name = "P2";
                salesPersons.First().Groups = new List<string> { "C", "B" };
                salesPersons.First().IsAssigned = !salesPersons.First().IsAssigned;

                await repo.SaveChanges();

                using (var reader = File.OpenText(dataFile))
                {
                    var fileData = await reader.ReadToEndAsync();
                    JsonConvert.SerializeObject(salesPersons, Formatting.Indented).Should().Be(fileData);
                }
                File.Delete(dataFile);
            }
        }
    }
}
