﻿using FluentAssertions;
using FluentAssertions.Execution;
using SalesPersonAllocation.Domain.Models;
using Xunit;

namespace SalesPersonAllocation.Domain.Test
{
    public class SalesPersonTests
    {        
        public class Assign
        {
            [Fact]
            public void WhenNotAssignedYetShouldSuccesful()
            {
                var salesPerson = new SalesPerson();

                using (new AssertionScope())
                {
                    salesPerson.Assign().Should().BeTrue();
                    salesPerson.IsAssigned.Should().BeTrue();
                }
            }

            [Fact]
            public void WhenAlredyAssignedShouldSuccesful()
            {
                var salesPerson = new SalesPerson();
                salesPerson.Assign();

                salesPerson.Assign().Should().BeFalse();
            }
        }        
    }
}
