﻿using FluentAssertions;
using FluentAssertions.Execution;
using SalespersonAllocation.Domain;
using SalespersonAllocation.Domain.Enums;
using SalesPersonAllocation.Domain.Commands;
using System.Linq;
using Xunit;

namespace SalesPersonAllocation.Domain.Test.Command
{
    public class AssignSalesPersonCommandTests
    {
        public class Specialties
        {
            [Fact]
            public void ShouldReturnListOfSelectedSpecialties()
            {
                var command = new AssignSalesPersonCommand
                {
                    Language = SpecialtyLanguage.Greek,
                    Vehicle = SpecialtyVehicle.Family
                };

                var specialties = command.Specialties();

                using (new AssertionScope())
                {
                    specialties.Should().HaveCount(2);
                    specialties.Any(s => s.IsSame(new Specialty(SpecialtyType.Vehicle, (int) SpecialtyVehicle.Family))).Should().BeTrue();
                    specialties.Any(s => s.IsSame(new Specialty(SpecialtyType.Language, (int) SpecialtyLanguage.Greek))).Should().BeTrue();
                }
            }

            [Fact]
            public void WhenVehicaleIsTradieShouldNotConsiderLanguage()
            {
                var command = new AssignSalesPersonCommand
                {
                    Language = SpecialtyLanguage.Greek,
                    Vehicle = SpecialtyVehicle.Tradie
                };

                var specialties = command.Specialties();

                using (new AssertionScope())
                {
                    specialties.Should().HaveCount(1);
                    specialties.Any(s => s.IsSame(new Specialty(SpecialtyType.Vehicle, (int)SpecialtyVehicle.Tradie))).Should().BeTrue();                    
                }
            }
        }
    }
}
