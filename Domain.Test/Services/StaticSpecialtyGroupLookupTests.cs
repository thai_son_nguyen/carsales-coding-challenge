﻿using FluentAssertions;
using SalespersonAllocation.Domain.Enums;
using SalespersonAllocation.Domain.Services;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace SalesPersonAllocation.Domain.Test.Services
{
    public class StaticSpecialtyGroupLookupTests
    {
        public class GroupNamesFromSpecialty
        {
            [Theory]
            [MemberData(nameof(Data))]
            public void ShouldReturnCorrectGroups(SalespersonAllocation.Domain.Specialty specialty, string expected)
            {
                var lookup = new StaticSpecialtyGroupLookup();

                var actual = lookup.GroupFrom(specialty);

                actual.Should().Be(expected);
            }

            public static IEnumerable<object[]> Data => new List<object[]>
            {
                new object[] {
                    new SalespersonAllocation.Domain.Specialty(SpecialtyType.Language, (int)SpecialtyLanguage.Greek),
                    "A"
                },
                new object[] {
                    new SalespersonAllocation.Domain.Specialty(SpecialtyType.Vehicle, (int)SpecialtyVehicle.Sports),
                    "B"
                },
                new object[] {
                    new SalespersonAllocation.Domain.Specialty(SpecialtyType.Vehicle, (int)SpecialtyVehicle.Family),
                    "C"
                },
                new object[] {
                    new SalespersonAllocation.Domain.Specialty(SpecialtyType.Vehicle, (int)SpecialtyVehicle.Tradie),
                    "D"
                },
                new object[] {
                    new SalespersonAllocation.Domain.Specialty(SpecialtyType.Vehicle, 999),
                    string.Empty
                },
                new object[] {
                    null,
                    string.Empty
                }
            };
        }
    }
}
