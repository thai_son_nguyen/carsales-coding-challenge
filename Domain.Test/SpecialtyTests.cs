﻿using FluentAssertions;
using SalespersonAllocation.Domain.Enums;
using System.Collections.Generic;
using Xunit;

namespace SalesPersonAllocation.Domain.Test
{
    public class SpecialtyTests
    {
        public class IsSame
        {
            [Fact]
            public void WhenTheOtherIsNotSameTypeShouldNotBeTrue()
            {
                var specialty = new SalespersonAllocation.Domain.Specialty(SpecialtyType.Language, 1);

                var other = new SalespersonAllocation.Domain.Specialty(SpecialtyType.Vehicle, 1);

                specialty.IsSame(other).Should().BeFalse();
            }

            [Fact]
            public void WhenTheOtherSpecialtyHasDifferentValueShouldNotBeTrue()
            {
                var specialty = new SalespersonAllocation.Domain.Specialty(SpecialtyType.Vehicle, 1);

                var other = new SalespersonAllocation.Domain.Specialty(SpecialtyType.Vehicle, 2);

                specialty.IsSame(other).Should().BeFalse();
            }

            [Fact]
            public void WhenTheOtherSpecialtyHasSameTypeAndValueShouldBeTrue()
            {
                var specialty = new SalespersonAllocation.Domain.Specialty(SpecialtyType.Vehicle, 2);

                var other = new SalespersonAllocation.Domain.Specialty(SpecialtyType.Vehicle, 2);

                specialty.IsSame(other).Should().BeTrue();
            }
        }
    }
}
