﻿using FluentAssertions;
using FluentAssertions.Execution;
using Moq;
using SalespersonAllocation.Domain;
using SalespersonAllocation.Domain.Enums;
using SalespersonAllocation.Domain.Ranking;
using SalespersonAllocation.Domain.Repositories;
using SalespersonAllocation.Domain.Services;
using SalesPersonAllocation.Domain.Commands;
using SalesPersonAllocation.Domain.Handlers;
using SalesPersonAllocation.Domain.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace SalesPersonAllocation.Domain.Test.Handlers
{
    public class AssignSalesPersonHandlerTests
    {
        private Mock<IRepository<SalesPerson>> repositoryMock;
        private Mock<ISpecialtyRankingStrategy> specialtyRankingStrategy;
        private AssignSalesPersonHandler handler;
        private AssignSalesPersonCommand command;
        private List<SalesPerson> salesPersons;
        private IOrderedEnumerable<SalesPerson> salesPersonsOrdered;
        private Mock<ISpecialtyGroupRanker> rankerMock;

        public AssignSalesPersonHandlerTests()
        {
            command = new AssignSalesPersonCommand
            {
                Language = SpecialtyLanguage.Greek,
                Vehicle = SpecialtyVehicle.Family
            };            
            salesPersons = new List<SalesPerson>();
            salesPersonsOrdered= salesPersons.OrderByDescending(_ => 0);

            specialtyRankingStrategy = new Mock<ISpecialtyRankingStrategy>();
            rankerMock = new Mock<ISpecialtyGroupRanker>();
            rankerMock.Setup(r => r.ApplyRankOrder(salesPersons)).Returns(salesPersonsOrdered);
            specialtyRankingStrategy.Setup(s => s.BuildRankerFor(It.IsAny<List<Specialty>>())).Returns(rankerMock.Object);

            repositoryMock = new Mock<IRepository<SalesPerson>>();
            repositoryMock.Setup(r => r.Entities()).ReturnsAsync(salesPersons.AsQueryable());

            handler = new AssignSalesPersonHandler(repositoryMock.Object, specialtyRankingStrategy.Object);
        }

        [Fact]
        public async Task ShouldUseSpecialtiesFromCommand()
        {
            List<Specialty> usedSpecialties = null;

            specialtyRankingStrategy.Setup(s => s.BuildRankerFor(It.IsAny<List<Specialty>>())).Returns(rankerMock.Object).Callback<List<Specialty>>(specialties =>
            {
                usedSpecialties = specialties;
            });

            await handler.Handle(command);

            using (new AssertionScope())
            {
                var expected = command.Specialties();
                usedSpecialties.Should().HaveCount(expected.Count);
                usedSpecialties.All(us => expected.Any(e => e.IsSame(us))).Should().BeTrue();
            }
        }

        [Fact]
        public async Task ShouldFilterOutAlreadyAssignedPersons()
        {
            salesPersons.AddRange(new List<SalesPerson>
            {
                new SalesPerson
                {
                    Name = "P1",
                    IsAssigned = true
                }
            });

            var result = await handler.Handle(command);

            result.Should().BeNull();
        }


        [Fact]
        public async Task ShouldGetFirstAvailablePersons()
        {
            salesPersons.AddRange(new List<SalesPerson>
            {
                new SalesPerson
                {
                    Name = "P1"
                },
                new SalesPerson
                {
                    Name = "P2"
                }
            });

            var result = await handler.Handle(command);

            result.Should().Be(salesPersons[0]);
        }


        [Fact]
        public async Task ShouldSetTheFoundPersonAsAssigned()
        {
            salesPersons.AddRange(new List<SalesPerson>
            {
                new SalesPerson
                {
                    Name = "P1"
                },
                new SalesPerson
                {
                    Name = "P2"
                }
            });

            var result = await handler.Handle(command);

            result.IsAssigned.Should().BeTrue();
        }

        [Fact]
        public async Task ShouldCallRepositoryToSave()
        {
            salesPersons.AddRange(new List<SalesPerson>
            {
                new SalesPerson
                {
                    Name = "P1"
                },
                new SalesPerson
                {
                    Name = "P2"
                }
            });

            await handler.Handle(command);
            

            repositoryMock.Verify(r => r.SaveChanges(), Times.Once);
        }

        [Fact]
        public async Task WhenNotFoundShouldNotCallRepositoryToSave()
        {
            await handler.Handle(command);
            repositoryMock.Verify(r => r.SaveChanges(), Times.Never);
        }

        public class Integrations
        {
            private Mock<IRepository<SalesPerson>> repositoryMock;
            private static List<SalesPerson> persons = new List<SalesPerson>
                {
                    new SalesPerson
                    {
                        Name = "Cierra Vega",
                        Groups = new List<string> { "A" }
                    },
                    new SalesPerson
                    {
                        Name = "Alden Cantrell",
                        Groups = new List<string> { "B", "D" }
                    },
                    new SalesPerson
                    {
                        Name = "Kierra Gentry",
                        Groups = new List<string> { "A", "C" }
                    },
                    new SalesPerson
                    {
                        Name = "Pierre Cox",
                        Groups = new List<string> { "D" }
                    },
                    new SalesPerson
                    {
                        Name = "Thomas Crane",
                        Groups = new List<string> { "A", "B" }
                    },
                    new SalesPerson
                    {
                        Name = "Miranda Shaffer",
                        Groups = new List<string> { "B" }
                    },
                    new SalesPerson
                    {
                        Name = "Bradyn Kramer",
                        Groups = new List<string> { "D" }
                    },
                    new SalesPerson
                    {
                        Name = "Alvaro Mcgee",
                        Groups = new List<string> { "A", "D", "C" }
                    }
                };

            private AssignSalesPersonHandler handler;

            public Integrations()
            {
                repositoryMock = new Mock<IRepository<SalesPerson>>();

                repositoryMock.Setup(r => r.Entities()).ReturnsAsync(persons.AsQueryable());

                handler = new AssignSalesPersonHandler(repositoryMock.Object, new VehicleBeforeLanguageRankingStrategy(new StaticSpecialtyGroupLookup()));
            }

            [Theory]
            [MemberData(nameof(Data))]
            public async Task ShouldPickBestMatchAvailablePersons(AssignSalesPersonCommand command, string expectedPersonName)
            {
                var assigned = await handler.Handle(command);

                assigned.Name.Should().Be(expectedPersonName);
            }

            public static IEnumerable<object[]> Data => new List<object[]>
            {
                new object[] {
                    new AssignSalesPersonCommand {
                        Language = SpecialtyLanguage.Greek,
                        Vehicle = SpecialtyVehicle.Family
                    },
                    "Kierra Gentry"
                },
                new object[] {
                    new AssignSalesPersonCommand {
                        Language = SpecialtyLanguage.Greek,
                        Vehicle = SpecialtyVehicle.Sports
                    },
                    "Thomas Crane"
                },
                new object[] {
                    new AssignSalesPersonCommand {
                        Language = SpecialtyLanguage.Any,
                        Vehicle = SpecialtyVehicle.Sports
                    },
                    "Alden Cantrell"
                },
                new object[] {
                    new AssignSalesPersonCommand {
                        Language = SpecialtyLanguage.Greek,
                        Vehicle = SpecialtyVehicle.Any
                    },
                    "Cierra Vega"
                },
                 new object[] {
                    new AssignSalesPersonCommand {
                        Language = SpecialtyLanguage.Any,
                        Vehicle = SpecialtyVehicle.Any
                    },
                    "Pierre Cox"
                }
            };
        }
    }
}
