﻿using FluentAssertions;
using SalespersonAllocation.Domain.Ranking;
using SalesPersonAllocation.Domain.Models;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace SalesPersonAllocation.Domain.Test.Ranking
{
    public class SpecialtyGroupRankerTests
    {
        public class Initialize
        {
            [Fact]
            public void ShouldSetCorrectGroup()
            {
                var ranker = new SpecialtyGroupRanker("C");
                ranker.Group.Should().Be("C");
            }

        }
        public class AddSuccesor
        {
            [Fact]
            public void WhenThereIsNoSuccesorShouldAddSuccesorForItSelf()
            {
                var ranker = new SpecialtyGroupRanker("C");
                var succesor = new SpecialtyGroupRanker("A");

                ranker.AddSuccesor(succesor);

                ranker.Succesor.Should().Be(succesor);
                succesor.Succesor.Should().BeNull();
            }

            [Fact]
            public void WhenThereIsAlreadySuccesorShouldAddSuccesorToBottomOfTheChain()
            {
                var ranker = new SpecialtyGroupRanker("C");
                var succesor = new SpecialtyGroupRanker("A");
                var succesorLast = new SpecialtyGroupRanker("E");

                ranker.AddSuccesor(succesor);
                ranker.AddSuccesor(succesorLast);

                ranker.Succesor.Should().Be(succesor);
                succesor.Succesor.Should().Be(succesorLast);
            }
        }

        public class ApplyRankOrderToIQueryable
        {
            [Fact]
            public void ShouldApplyCorrectOrderByClause()
            {
                var salesPersons = new List<SalesPerson>
                {
                    new SalesPerson
                    {
                        Name = "P1",
                        IsAssigned = true,
                        Groups = new List<string> { "B" }
                    },
                    new SalesPerson
                    {
                        Name = "P2",
                        IsAssigned = true,
                        Groups = new List<string> { "C" }
                    },
                    new SalesPerson
                    {
                        Name = "P3",
                        IsAssigned = true,
                        Groups = new List<string> { "A" }
                    }
                };

                var ranker = new SpecialtyGroupRanker("C");                
                var sortedResult = ranker.ApplyRankOrder(salesPersons.AsQueryable()).ToList();

                string.Join(",", sortedResult.Select(p => p.Name)).Should().Be("P2,P1,P3");
            }

            [Fact]
            public void WhenHaveSuccessorShouldCallSuccesor()
            {
                var salesPersons = new List<SalesPerson>
                {
                    new SalesPerson
                    {
                        Name = "P1",
                        IsAssigned = true,
                        Groups = new List<string> { "B" }
                    },
                    new SalesPerson
                    {
                        Name = "P2",
                        IsAssigned = true,
                        Groups = new List<string> { "C" }
                    },
                    new SalesPerson
                    {
                        Name = "P3",
                        IsAssigned = true,
                        Groups = new List<string> { "A" }
                    }
                };

                var ranker = new SpecialtyGroupRanker("C");
                var succesor = new SpecialtyGroupRanker("A");
                ranker.AddSuccesor(succesor);

                var sortedResult = ranker.ApplyRankOrder(salesPersons.AsQueryable()).ToList();

                string.Join(",", sortedResult.Select(p => p.Name)).Should().Be("P2,P3,P1");
            }
        }

        public class ApplyRankOrderToIOrderedQueryable
        {
            [Fact]
            public void ShouldChainCorrectOrderByClause()
            {
                var salesPersons = new List<SalesPerson>
                {
                    new SalesPerson
                    {
                        Name = "P1",
                        IsAssigned = true,
                        Groups = new List<string> { "B" }
                    },
                    new SalesPerson
                    {
                        Name = "P2",
                        IsAssigned = true,
                        Groups = new List<string> { "C" }
                    },
                    new SalesPerson
                    {
                        Name = "P3",
                        IsAssigned = false,
                        Groups = new List<string> { "A" }
                    }
                };

                var ranker = new SpecialtyGroupRanker("C");
                var sortedResult = ranker.ApplyRankOrder(salesPersons.OrderBy(p => p.IsAssigned)).ToList();

                string.Join(",", sortedResult.Select(p => p.Name)).Should().Be("P3,P2,P1");
            }

            [Fact]
            public void WhenHaveSuccessorShouldCallSuccesor()
            {
                var salesPersons = new List<SalesPerson>
                {
                    new SalesPerson
                    {
                        Name = "P1",
                        IsAssigned = true,
                        Groups = new List<string> { "B" }
                    },
                    new SalesPerson
                    {
                        Name = "P2",
                        IsAssigned = true,
                        Groups = new List<string> { "C" }
                    },
                    new SalesPerson
                    {
                        Name = "P3",
                        IsAssigned = false,
                        Groups = new List<string> { "A" }
                    },
                    new SalesPerson
                    {
                        Name = "P4",
                        IsAssigned = true,
                        Groups = new List<string> { "D" }
                    },
                };

                var ranker = new SpecialtyGroupRanker("C");
                var succesor = new SpecialtyGroupRanker("D");
                ranker.AddSuccesor(succesor);
                var sortedResult = ranker.ApplyRankOrder(salesPersons.OrderBy(p => p.IsAssigned)).ToList();

                string.Join(",", sortedResult.Select(p => p.Name)).Should().Be("P3,P2,P4,P1");
            }
        }
    }
}
