﻿using FluentAssertions;
using FluentAssertions.Execution;
using Moq;
using SalespersonAllocation.Domain;
using SalespersonAllocation.Domain.Enums;
using SalespersonAllocation.Domain.Ranking;
using SalespersonAllocation.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace SalesPersonAllocation.Domain.Test.Ranking
{
    public class VehicleBeforeLanguageRankingStrategyTests
    {
        Mock<ISpecialtyGroupLookup> lookupMock;
        VehicleBeforeLanguageRankingStrategy strategy;

        public VehicleBeforeLanguageRankingStrategyTests()
        {
            lookupMock = new Mock<ISpecialtyGroupLookup>();
            strategy = new VehicleBeforeLanguageRankingStrategy(lookupMock.Object);
        }

        [Fact]
        public void ShouldCheckForVehicleFirst()
        {
            var specialties = new List<Specialty> {
                new Specialty(SpecialtyType.Language, (int) SpecialtyLanguage.Any),
                new Specialty(SpecialtyType.Vehicle, (int) SpecialtyVehicle.Sports)
            };

            lookupMock.Setup(l => l.GroupFrom(specialties[0])).Returns("A");
            lookupMock.Setup(l => l.GroupFrom(specialties[1])).Returns("B");

            var ranker = strategy.BuildRankerFor(specialties);

            using(new AssertionScope())
            {
                ranker.Should().BeOfType<SpecialtyGroupRanker>();
                ((SpecialtyGroupRanker)ranker).Group.Should().Be("B");
            }
        }

        [Fact]
        public void ShouldCheckForLanguageAfterVehicle()
        {
            var specialties = new List<Specialty> {
                new Specialty(SpecialtyType.Language, (int) SpecialtyLanguage.Any),
                new Specialty(SpecialtyType.Vehicle, (int) SpecialtyVehicle.Sports)
            };

            lookupMock.Setup(l => l.GroupFrom(specialties[0])).Returns("A");
            lookupMock.Setup(l => l.GroupFrom(specialties[1])).Returns("B");

            var ranker = strategy.BuildRankerFor(specialties);

            using (new AssertionScope())
            {
                ranker.Succesor.Should().BeOfType<SpecialtyGroupRanker>();
                ((SpecialtyGroupRanker)ranker.Succesor).Group.Should().Be("A");
            }
        }

        [Fact]
        public void ShouldOnlyCreateRequiedSpecialties()
        {
            var specialties = new List<Specialty> {
                new Specialty(SpecialtyType.Language, (int) SpecialtyLanguage.Any),
                new Specialty(SpecialtyType.Vehicle, (int) SpecialtyVehicle.Sports),
            };

            lookupMock.Setup(l => l.GroupFrom(specialties[0])).Returns("A");
            lookupMock.Setup(l => l.GroupFrom(specialties[1])).Returns("B");

            var ranker = strategy.BuildRankerFor(specialties);

            ranker.Succesor.Succesor.Should().BeNull();
        }

        [Fact]
        public void WhenThereIsNoGivenSpecialiesShouldReturnNullRanker()
        {
            var specialties = new List<Specialty>();

            var ranker = strategy.BuildRankerFor(specialties);

            ranker.Should().BeOfType<NullRanker>();
        }

        [Fact]
        public void WhenInputIsNullShouldReturnNullRanker()
        {
            var ranker = strategy.BuildRankerFor(null);

            ranker.Should().BeOfType<NullRanker>();
        }
    }
}
