﻿using FluentAssertions;
using SalespersonAllocation.Domain.Ranking;
using SalesPersonAllocation.Domain.Models;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace SalesPersonAllocation.Domain.Test.Ranking
{
    public class NullRankerTests
    {
        public class AddSuccesor
        {
            [Fact]
            public void ShouldDoNothing()
            {
                var ranker = new NullRanker();
                var succesor = new SpecialtyGroupRanker("A");

                ranker.AddSuccesor(succesor);

                ranker.Succesor.Should().BeNull();                
            }           
        }

        public class ApplyRankOrderToIQueryable
        {
            [Fact]
            public void ShouldDoNothing()
            {
                var salesPersons = new List<SalesPerson>
                {
                    new SalesPerson
                    {
                        Name = "P1",
                        IsAssigned = true,
                        Groups = new List<string> { "B" }
                    },
                    new SalesPerson
                    {
                        Name = "P2",
                        IsAssigned = true,
                        Groups = new List<string> { "C" }
                    },
                    new SalesPerson
                    {
                        Name = "P3",
                        IsAssigned = true,
                        Groups = new List<string> { "A" }
                    }
                };

                var ranker = new NullRanker();                
                var sortedResult = ranker.ApplyRankOrder(salesPersons.AsQueryable()).ToList();

                string.Join(",", sortedResult.Select(p => p.Name)).Should().Be("P1,P2,P3");
            }          
        }

        public class ApplyRankOrderToIOrderedQueryable
        {
            [Fact]
            public void ShouldChainCorrectOrderByClause()
            {
                var salesPersons = new List<SalesPerson>
                {
                    new SalesPerson
                    {
                        Name = "P1",
                        IsAssigned = true,
                        Groups = new List<string> { "B" }
                    },
                    new SalesPerson
                    {
                        Name = "P2",
                        IsAssigned = true,
                        Groups = new List<string> { "C" }
                    },
                    new SalesPerson
                    {
                        Name = "P3",
                        IsAssigned = false,
                        Groups = new List<string> { "A" }
                    }
                };

                var ranker = new NullRanker();                                
                var sortedResult = ranker.ApplyRankOrder(salesPersons.AsQueryable().OrderBy(p => p.IsAssigned)).ToList();

                string.Join(",", sortedResult.Select(p => p.Name)).Should().Be("P3,P1,P2");
            }
        }
    }
}
