﻿using SalesPersonAllocation.Domain.Models;
using System.Collections.Generic;
using System.Linq;

namespace SalespersonAllocation.Domain.Ranking
{
    public class NullRanker : ISpecialtyGroupRanker
    {
        public ISpecialtyGroupRanker Succesor { get; }

        public void AddSuccesor(ISpecialtyGroupRanker nexSuccesor)
        {            
        }

        public IOrderedEnumerable<SalesPerson> ApplyRankOrder(IEnumerable<SalesPerson> salesPersons)
        {
            return salesPersons.OrderByDescending(s => 0);
        }

        public IOrderedEnumerable<SalesPerson> ApplyRankOrder(IOrderedEnumerable<SalesPerson> salesPersons)
        {
            return salesPersons.OrderByDescending(s => 0);
        }
    }
}
