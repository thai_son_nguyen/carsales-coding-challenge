﻿using SalesPersonAllocation.Domain.Models;
using System.Collections.Generic;
using System.Linq;

namespace SalespersonAllocation.Domain.Ranking
{
    public interface ISpecialtyGroupRanker
    {
        ISpecialtyGroupRanker Succesor { get; }
        void AddSuccesor(ISpecialtyGroupRanker nexSuccesor);
        IOrderedEnumerable<SalesPerson> ApplyRankOrder(IEnumerable<SalesPerson> salesPersons);
        IOrderedEnumerable<SalesPerson> ApplyRankOrder(IOrderedEnumerable<SalesPerson> salesPersons);
    }
}
