﻿using SalespersonAllocation.Domain.Enums;
using SalespersonAllocation.Domain.Services;
using System.Collections.Generic;
using System.Linq;

namespace SalespersonAllocation.Domain.Ranking
{
    public class VehicleBeforeLanguageRankingStrategy : ISpecialtyRankingStrategy
    {
        private ISpecialtyGroupLookup specialtyGroupLookup;

        public VehicleBeforeLanguageRankingStrategy(ISpecialtyGroupLookup specialtyGroupLookup)
        {
            this.specialtyGroupLookup = specialtyGroupLookup;
        }

        public ISpecialtyGroupRanker BuildRankerFor(List<Specialty> specialties)
        {
            ISpecialtyGroupRanker topRanker = null;            

            if (specialties != null)
            {                
                foreach (var ranker in specialties
                    .OrderByDescending(s => s.Type == SpecialtyType.Vehicle)
                    .ThenByDescending(s => s.Type == SpecialtyType.Language)
                    .Select(s => new SpecialtyGroupRanker(specialtyGroupLookup.GroupFrom(s))))
                {
                    if (topRanker == null) {
                        topRanker = ranker;
                    } 
                    else
                    {
                        topRanker.AddSuccesor(ranker);
                    }
                }
            }
            return topRanker ?? new NullRanker();
        }
    }
}
