﻿using System.Collections.Generic;

namespace SalespersonAllocation.Domain.Ranking
{
    public interface ISpecialtyRankingStrategy
    {
        ISpecialtyGroupRanker BuildRankerFor(List<Specialty> specialties);
    }
}
