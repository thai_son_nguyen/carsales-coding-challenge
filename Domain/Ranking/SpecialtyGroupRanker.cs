﻿using SalesPersonAllocation.Domain.Models;
using System.Collections.Generic;
using System.Linq;

namespace SalespersonAllocation.Domain.Ranking
{
    public class SpecialtyGroupRanker : ISpecialtyGroupRanker
    {
        public string Group { get; private set; }

        public ISpecialtyGroupRanker Succesor { get; protected set; }

        public SpecialtyGroupRanker(string group)
        {
            Group = group;
        }

        public void AddSuccesor(ISpecialtyGroupRanker nexSuccesor)
        {
            if (Succesor != null)
            {
                Succesor.AddSuccesor(nexSuccesor);
            }
            else
            {
                Succesor = nexSuccesor;
            }
        }

        public IOrderedEnumerable<SalesPerson> ApplyRankOrder(IEnumerable<SalesPerson> salesPersons)
        {
            var result = salesPersons.OrderByDescending(p => p.Groups.Contains(Group));
            if (Succesor != null)
            {
                return Succesor.ApplyRankOrder(result);
            }

            return result;
        }

        public IOrderedEnumerable<SalesPerson> ApplyRankOrder(IOrderedEnumerable<SalesPerson> salesPersons)
        {
            var result = salesPersons.ThenByDescending(p => p.Groups.Contains(Group));
            if (Succesor != null)
            {
                return Succesor.ApplyRankOrder(result);
            }

            return result;
        }
    }
}
