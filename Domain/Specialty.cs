﻿using SalespersonAllocation.Domain.Enums;

namespace SalespersonAllocation.Domain
{
    public class Specialty
    {
        public SpecialtyType Type { get; set; }
        public int Value { get; set; }

        public Specialty(SpecialtyType type, int value)
        {
            Type = type;
            Value = value;
        }

        public bool IsSame(Specialty other) => Type == other.Type && Value == other.Value;
    }
}
