﻿using System.Collections.Generic;
using System.Linq;

namespace SalesPersonAllocation.Domain.Models
{
    public class SalesPerson
    {
        public IReadOnlyCollection<string> Groups { get; set; }

        public string Name { get; set; }

        public bool IsAssigned { get; set; }

        public bool Assign()
        {
            if (!IsAssigned)
            {
                IsAssigned = true;
                return true;
            }
            return false;
        }       
    }
}
