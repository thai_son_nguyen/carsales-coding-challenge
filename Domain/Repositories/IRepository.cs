﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalespersonAllocation.Domain.Repositories
{
    public interface IRepository<T> where T : class
    {
        Task SaveChanges();

        Task<IEnumerable<T>> Entities();
    }
}
