﻿namespace SalespersonAllocation.Domain.Enums
{
    public enum SpecialtyLanguage
    {
        Any,
        Greek,
    }

    public enum SpecialtyVehicle
    {
        Any,
        Sports,
        Family,
        Tradie
    }
}
