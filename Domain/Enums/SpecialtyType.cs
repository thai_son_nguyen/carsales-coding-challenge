﻿namespace SalespersonAllocation.Domain.Enums
{
    public enum SpecialtyType
    {
        Language,
        Vehicle
    }
}
