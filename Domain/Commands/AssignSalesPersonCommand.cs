﻿using SalespersonAllocation.Domain;
using SalespersonAllocation.Domain.Enums;
using System.Collections.Generic;

namespace SalesPersonAllocation.Domain.Commands
{
    public class AssignSalesPersonCommand
    {        
        public SpecialtyLanguage Language { get; set; }
        public SpecialtyVehicle Vehicle { get; set; }

        public AssignSalesPersonCommand() { }

        public List<Specialty> Specialties()
        {
            var specialties = new List<Specialty>
            {
                new Specialty(SpecialtyType.Vehicle, (int) Vehicle),
            };

            if (Vehicle != SpecialtyVehicle.Tradie)
            {
                specialties.Add(new Specialty(SpecialtyType.Language, (int)Language));
            }

            return specialties;
        }
    }
}
