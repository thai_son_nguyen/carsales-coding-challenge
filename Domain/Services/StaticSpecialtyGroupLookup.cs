﻿using SalespersonAllocation.Domain.Enums;
using System.Collections.Generic;
using System.Linq;

namespace SalespersonAllocation.Domain.Services
{
    public class StaticSpecialtyGroupLookup : ISpecialtyGroupLookup
    {
        public string GroupFrom(Specialty specialty)
        {
            string match = null;
            if (specialty != null)
                match =  SpecialtyLookup.Keys.Where(group => SpecialtyLookup[group].IsSame(specialty)).FirstOrDefault();

            return match ?? string.Empty;
        }

        private static readonly Dictionary<string, Specialty> SpecialtyLookup = new Dictionary<string, Specialty>
        {
            { "A", new Specialty(SpecialtyType.Language, (int) SpecialtyLanguage.Greek) },
            { "B", new Specialty(SpecialtyType.Vehicle, (int)SpecialtyVehicle.Sports) },
            { "C", new Specialty(SpecialtyType.Vehicle, (int)SpecialtyVehicle.Family) },
            { "D", new Specialty(SpecialtyType.Vehicle, (int)SpecialtyVehicle.Tradie) },
        };
    }
}
