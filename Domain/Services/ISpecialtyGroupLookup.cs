﻿namespace SalespersonAllocation.Domain.Services
{
    public interface ISpecialtyGroupLookup
    {
        string GroupFrom(Specialty specialty);
    }
}
