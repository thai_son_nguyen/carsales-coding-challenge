﻿using System.Threading.Tasks;

namespace SalesPersonAllocation.Domain.Handlers
{
    public interface ICommandHandler<TCommand, TResult>
    {
        Task<TResult> Handle(TCommand command);
    }
}
