﻿using SalespersonAllocation.Domain.Ranking;
using SalespersonAllocation.Domain.Repositories;
using SalesPersonAllocation.Domain.Commands;
using SalesPersonAllocation.Domain.Models;
using System.Linq;
using System.Threading.Tasks;

namespace SalesPersonAllocation.Domain.Handlers
{
    public class AssignSalesPersonHandler : ICommandHandler<AssignSalesPersonCommand, SalesPerson>
    {
        private readonly IRepository<SalesPerson> repository;
        private readonly ISpecialtyRankingStrategy specialtyRankingStrategy;

        public AssignSalesPersonHandler(
            IRepository<SalesPerson> repository, ISpecialtyRankingStrategy specialtyRankingStrategy)
        {
            this.repository = repository;
            this.specialtyRankingStrategy = specialtyRankingStrategy;
        }

        public async Task<SalesPerson> Handle(AssignSalesPersonCommand command)
        {
            var persons = await repository.Entities();
            var ranker = specialtyRankingStrategy.BuildRankerFor(command.Specialties());
            
            var person = ranker.ApplyRankOrder(persons).Where(p => !p.IsAssigned).FirstOrDefault();

            if (person != null)
            {
                person.Assign();
                await repository.SaveChanges();
            }

            return person;
        }
    }
}
