﻿using Microsoft.AspNetCore.Mvc;
using SalesPersonAllocation.Api.ViewModels;
using SalesPersonAllocation.Domain.Commands;
using SalesPersonAllocation.Domain.Handlers;
using SalesPersonAllocation.Domain.Models;
using System.Threading.Tasks;

namespace SalesPersonAllocation.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SalesPersonController : ControllerBase
    {
        private ICommandHandler<AssignSalesPersonCommand, SalesPerson> assignSalesPersonCommandHandler;

        public SalesPersonController(ICommandHandler<AssignSalesPersonCommand, SalesPerson> assignSalesPersonCommandHandler)
        {
            this.assignSalesPersonCommandHandler = assignSalesPersonCommandHandler;
        }

        [HttpGet]
        [Route("specialties")]
        public IActionResult GetSalesPersonSpecialties()
        {
            return new OkObjectResult(new SalesPersonSpecialtiesResponse());
        }

        [HttpPost]
        [Route("assign")]
        public async Task<IActionResult> AssignSalesPerson(AssignSalesPersonCommand command)
        {
            var person = await assignSalesPersonCommandHandler.Handle(command);
            return person != null ? new OkObjectResult(AssignSalesPersonResponse.From(person)) : (IActionResult)new NotFoundResult();
        }
    }
}
