﻿using SalesPersonAllocation.Domain.Models;

namespace SalesPersonAllocation.Api.ViewModels
{
    public class AssignSalesPersonResponse
    {
        public string Name { get; set; }

        private AssignSalesPersonResponse() { }

        public static AssignSalesPersonResponse From(SalesPerson salesPerson) => new AssignSalesPersonResponse { Name = salesPerson.Name };
    }
}
