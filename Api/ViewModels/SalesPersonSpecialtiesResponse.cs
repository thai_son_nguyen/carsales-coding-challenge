﻿
using SalespersonAllocation.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SalesPersonAllocation.Api.ViewModels
{
    public class SalesPersonSpecialtiesResponse
    {
        public List<KeyValuePair<string, string>> Languages => Enum.GetValues(typeof(SpecialtyLanguage))
            .OfType<SpecialtyLanguage>().OrderBy(v => (int)v).Select(v => new KeyValuePair<string, string>(((int)v).ToString(), v.ToString())).ToList();

        public List<KeyValuePair<string, string>> Vehicles => Enum.GetValues(typeof(SpecialtyVehicle))
            .OfType<SpecialtyVehicle>().OrderBy(v => (int)v).Select(v => new KeyValuePair<string, string>(((int)v).ToString(), v.ToString())).ToList();
    }
}
