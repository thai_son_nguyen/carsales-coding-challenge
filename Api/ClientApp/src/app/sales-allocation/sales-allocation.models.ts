interface AssignedSalesPerson {
  name: string;
}

interface AssignedSalesCommand {
  language: number;
  vehicle: number;
}

interface SalesPersonSpecialties {
  languages: [{
    Key: string
    Value: string
  }];
  vehicles: [{
    Key: string
    Value: string
  }];
}

interface SpecialtyOption {
  value: number;
  text: string;
}

