import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { SalesAllocationComponent } from './sales-allocation.component';
import { SalesAllocationService } from './sales-allocation.service';

const specialtiesResponse: SalesPersonSpecialties = {
  languages: [
    { Key: '0', Value: 'AAA' }
  ],
  vehicles: [
    { Key: '1', Value: 'CCC' }
  ],
};

const assignedSalesPerson: AssignedSalesPerson = { name: 'Person 1' };

class MockSalesAllocationService {

  public salesPersonSpecialties(): Observable<SalesPersonSpecialties> {
    return of(specialtiesResponse);
  }

  public assignSalesPerson(command): Observable<AssignedSalesPerson> {
    return of(assignedSalesPerson);
  }
}

describe('SalesAllocationComponent', () => {
  let fixture: ComponentFixture<SalesAllocationComponent>;
  let component: SalesAllocationComponent;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [
        SalesAllocationComponent
      ],
      imports: [FormsModule],
      providers: [
        { provide: SalesAllocationService, useClass: MockSalesAllocationService }
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(SalesAllocationComponent);
    component = fixture.componentInstance;
  });

  describe('Init data', () => {
    it('should init languages', () => {
      fixture.whenStable().then(() => { // wait for async getTodos
        fixture.detectChanges();        // update view with todos
        expect(component.languages[0]).toEqual({value: 0, text: 'AAA'});
      });
    });

    it('should init vehicles', () => {
      fixture.whenStable().then(() => { // wait for async getTodos
        fixture.detectChanges();        // update view with todos
        expect(component.vehicles[0]).toEqual({ value: 1, text: 'CCC' });
      });
    });

    it('should init command', () => {
      fixture.whenStable().then(() => { // wait for async getTodos
        fixture.detectChanges();        // update view with todos
        expect(component.command).toEqual({ language: 0, vehicle: 1});
      });
    });
  });

  describe('assignSalesPerson', () => {
    it('should update list of assigned sales person', () => {
      component.assignSalesPerson();
      expect(component.assignedPersons).toEqual([assignedSalesPerson]);
    });
  });
});
