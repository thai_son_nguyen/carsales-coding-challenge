import { Component, OnInit } from '@angular/core';
import { SalesAllocationService } from './sales-allocation.service';

@Component({
  selector: 'app-sales-allocation',
  templateUrl: './sales-allocation.component.html'
})
export class SalesAllocationComponent implements OnInit {
  public assignedPersons: AssignedSalesPerson[] = [];
  public command: AssignedSalesCommand = { language: 0, vehicle: 0 };
  public errorMessage = '';
  public languages: SpecialtyOption [];
  public vehicles: SpecialtyOption [];

  constructor(private service: SalesAllocationService) {
  }
    ngOnInit(): void {
      this.service.salesPersonSpecialties().subscribe(result => {
        this.languages = result.languages.map(l => ({ value: Number(l.Key), text: l.Value }));
        this.vehicles = result.vehicles.map(v => ({ value: Number(v.Key), text: v.Value }));
        this.command = { language: this.languages[0].value, vehicle: this.vehicles[0].value };
      }, error => this.errorMessage = error);
    }

  public assignSalesPerson() {
    this.service.assignSalesPerson(this.command).subscribe(result => {
      this.assignedPersons.push(result);
    }, error => this.errorMessage = error);
  }

  public ackError() {
    this.errorMessage = '';
  }
}
