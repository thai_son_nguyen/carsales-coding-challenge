import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SalesAllocationService {
  constructor(private http: HttpClient) { }

  private handleError(errorResponse: HttpErrorResponse) {
    switch (errorResponse.status) {
      case 404:
        return throwError('All salespeople are busy. Please wait.');
      default:
        return throwError('System error. Please contact support.');
    }
  }

  public assignSalesPerson(command: AssignedSalesCommand): Observable<AssignedSalesPerson> {
    return this.http.post<AssignedSalesPerson>(environment.baseApiUrl + 'SalesPerson/assign', command).pipe(
      catchError((errorResponse: HttpErrorResponse) => this.handleError(errorResponse))
    );
  }

  public salesPersonSpecialties(): Observable<SalesPersonSpecialties> {
    return this.http.get<SalesPersonSpecialties>(environment.baseApiUrl + 'SalesPerson/specialties').pipe(
      catchError(() => throwError('System error. Please contact support.'))
    );
  }
}
