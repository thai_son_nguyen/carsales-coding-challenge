﻿using FluentAssertions;
using FluentAssertions.Execution;
using SalespersonAllocation.Domain.Enums;
using SalesPersonAllocation.Api.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace SalesPersonAllocation.Api.Test.ViewModels
{
    public class SalesPersonSpecialtiesResponseTests
    {
        public class Languages
        {
            [Fact]
            public void ShouldIncludeAllLanguageSpecialties()
            {
                var model = new SalesPersonSpecialtiesResponse();

                var expected = Enum.GetValues(typeof(SpecialtyLanguage))
                    .OfType<SpecialtyLanguage>().OrderBy(v => (int)v).Select(v => new KeyValuePair<string, string>(((int)v).ToString(), v.ToString())).ToList();

                var actual = model.Languages;                

                using(new AssertionScope())
                {
                    actual.Should().HaveCount(expected.Count);
                    actual.All(a => expected.Any(e => e.Key == a.Key && e.Value == e.Value)).Should().BeTrue();
                }
            }
        }

        public class Vehicles
        {
            [Fact]
            public void ShouldIncludeAllVehicleSpecialties()
            {
                var model = new SalesPersonSpecialtiesResponse();

                var expected = Enum.GetValues(typeof(SpecialtyVehicle))
                    .OfType<SpecialtyVehicle>().OrderBy(v => (int)v).Select(v => new KeyValuePair<string, string>(((int)v).ToString(), v.ToString())).ToList();

                var actual = model.Vehicles;

                using (new AssertionScope())
                {
                    actual.Should().HaveCount(expected.Count);
                    actual.All(a => expected.Any(e => e.Key == a.Key && e.Value == e.Value)).Should().BeTrue();
                }
            }
        }
    }
}
