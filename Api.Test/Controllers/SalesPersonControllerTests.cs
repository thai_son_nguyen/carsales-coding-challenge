﻿using FluentAssertions;
using FluentAssertions.Execution;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Moq;
using Newtonsoft.Json;
using SalespersonAllocation.Domain;
using SalespersonAllocation.Domain.Enums;
using SalesPersonAllocation.Api.Controllers;
using SalesPersonAllocation.Api.ViewModels;
using SalesPersonAllocation.Domain.Commands;
using SalesPersonAllocation.Domain.Handlers;
using SalesPersonAllocation.Domain.Models;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SalesPersonAllocation.Api.Test.Controllers
{
    public class SalesPersonControllerTests
    {
        public class AssignSalesPersonSpecialties
        {
            private Mock<ICommandHandler<AssignSalesPersonCommand, SalesPerson>> handlerMock;
            private SalesPersonController controller;

            public AssignSalesPersonSpecialties()
            {
                handlerMock = new Mock<ICommandHandler<AssignSalesPersonCommand, SalesPerson>>();
                controller = new SalesPersonController(handlerMock.Object);
            }

            [Fact]
            public void ShouldReturnCorrectResult()
            {
                var result = controller.GetSalesPersonSpecialties();

                using (new AssertionScope())
                {
                    result.Should().BeOfType<OkObjectResult>();
                    var value = ((OkObjectResult)result).Value;
                    value.Should().BeOfType<SalesPersonSpecialtiesResponse>();
                }
            }
        }

        public class AssignSalesPerson
        {
            private Mock<ICommandHandler<AssignSalesPersonCommand, SalesPerson>> handlerMock;
            private SalesPersonController controller;

            public AssignSalesPerson()
            {
                handlerMock = new Mock<ICommandHandler<AssignSalesPersonCommand, SalesPerson>>();
                controller = new SalesPersonController(handlerMock.Object);
            }

            [Fact]
            public async Task WhenSuccesfulShouldReturnAssignedPersonDetails()
            {
                var command = new AssignSalesPersonCommand();
                var person = new SalesPerson
                {
                    Name = "ABC",
                    Groups = new List<string> { "A" }
                };

                handlerMock.Setup(h => h.Handle(command)).ReturnsAsync(person);

                var result = await controller.AssignSalesPerson(command);

                using(new AssertionScope())
                {
                    result.Should().BeOfType<OkObjectResult>();
                    var value = ((OkObjectResult)result).Value;
                    value.Should().BeOfType<AssignSalesPersonResponse>();
                    ((AssignSalesPersonResponse)value).Name.Should().Be(person.Name);
                }
            }

            [Fact]
            public async Task WhenNotSuccesfulShouldReturnNotFoundError()
            {
                var command = new AssignSalesPersonCommand();

                handlerMock.Setup(h => h.Handle(command)).ReturnsAsync((SalesPerson) null);

                var result = await controller.AssignSalesPerson(command);

                result.Should().BeOfType<NotFoundResult>();
            }


            public class ApiWebApplicationFactory : WebApplicationFactory<Startup>
            {
                protected override void ConfigureWebHost(IWebHostBuilder builder)
                {
                    // will be called after the `ConfigureServices` from the Startup
                    builder.ConfigureAppConfiguration((context, configBuilder) => {
                        configBuilder.AddInMemoryCollection(
                            new Dictionary<string, string>
                            {
                                ["DataFile"] = "salesperson-test.json"
                            });
                    });
                }
            }

            public class Integration:  IClassFixture<ApiWebApplicationFactory>
            {
                private HttpClient client;

                public Integration(ApiWebApplicationFactory fixture)
                {
                    client = fixture.CreateClient();
                }

                [Fact]
                public async Task ShouldPickTheFirstAvailableMatchingPerson()
                {
                    var command = new AssignSalesPersonCommand
                    {
                        Language = SpecialtyLanguage.Greek,
                        Vehicle = SpecialtyVehicle.Family
                    };
                    var httpContent = new StringContent(JsonConvert.SerializeObject(command), Encoding.UTF8, "application/json");

                    var response = await client.PostAsync("/SalesPerson/assign", httpContent);

                    using (new AssertionScope())
                    {
                        response.StatusCode.Should().Be(HttpStatusCode.OK);
                        var person = JsonConvert.DeserializeObject<AssignSalesPersonResponse>(await response.Content.ReadAsStringAsync());
                        person.Name.Should().Be("Kierra Gentry");
                    }
                }
            }
        }
    }
}
