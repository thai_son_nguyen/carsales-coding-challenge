## SYSTEM REQUIREMENTS
1. [.NET Core 3.1](https://dotnet.microsoft.com/download/dotnet/3.1)
2. [Node JS](https://nodejs.org/en/download/)
3. Tested in Windows

## DEVELOPMENT TOOL
1. [MS Visual Studio 2019](https://visualstudio.microsoft.com/downloads/)
---
## RUN INSTRUCTIONS
1. Clone the repo
2. Open the solution in Visual Studio
3. Select startup project as `SalesPersonAllocation.Api`
4. Run
5. Both Angular client app and web api will be run together
---
## ASSUMPTIONS

#### APPLICATION BEHAVIOURS
1. For convenience, json is place in folder `Data` by default. The path is configurable via entry `DataFile` in `appsettings.json`
2. After each assignment, assign status will be persisted to the file
3. There is no function to release assigned person. Please rollback changes in json file for fresh data

#### NOT IN SCOPE
1. Authentication and Authorization
2. Logging
3. Concurrency
4. Performance tuning
5. Greate UX
6. Amazing User Interface
7. Concurrency
---
## DESIGN CHOICES
1. Minimal error handling which means that exception will be thrown and error will be displayed to UI
2. The architecture could be over-engineer but it is extendable and for demonstration purpose.
3. I went with a simple solution which is to have the command handler doing the query instead of via ranker and strategy. However, I've refactored to make it is easier for future enhancement such as adding more specialties. That's my presumption about incoming requirements.
4. Tests include both unit tests and integration tests without mocking
5. For simplicity, Web API and Angular Client template was used to create the solution
6. I didn't have enough time to add all required tests for UI but only some tests to demonstrate how tests can be done with Angular
7. The UI is very robust
---
## FUTURE IMPROVEMENT
1. More tests for UI
2. Authentication and Authorization
3. Logging
4. Split client app from Web API
5. Simplify the solution a bit more if there is no additional requirement
6. Concurrency handling

### NOTES
- I haven't done any Angular before so all feedbacks are appreciated