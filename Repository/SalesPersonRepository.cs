﻿using Newtonsoft.Json;
using SalespersonAllocation.Domain.Repositories;
using SalesPersonAllocation.Domain.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Repository
{
    public class SalesPersonRepository : IRepository<SalesPerson>
    {
        private readonly string dataFile;
        private List<SalesPerson> salesPersons;

        public SalesPersonRepository(string dataFile)
        {
            this.dataFile = dataFile;
        }

        public async Task<IEnumerable<SalesPerson>> Entities()
        {
            using (var reader = File.OpenText(dataFile))
            {
                var fileText = await reader.ReadToEndAsync();
                salesPersons = JsonConvert.DeserializeObject<List<SalesPerson>>(fileText);
                return salesPersons.AsQueryable();
            }
        }

        public async Task SaveChanges()
        {
            using (StreamWriter writer = File.CreateText(dataFile))
            {
                await writer.WriteAsync(JsonConvert.SerializeObject(salesPersons, Formatting.Indented));
            }
        }
    }
}
